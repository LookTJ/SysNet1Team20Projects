/**
 * mt-collatz-method4
 * 
 * A program that utilizes multithreading to compute collatz
 * sequences between 2 and N. Method 4 works by having one thread
 * work on a single number and report the when all computation is completed
 *
 * @authors Annaly Benitez, Taylor Lookabaugh Andrew Petrovsky
 * @date 10 October 2017
 * @info COP4634
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h> 
#include <time.h>

#define MAXSIZE 1000   /* Maximum results array size */

/**
 * @breif Initialize
 * Sets up the local variables in refernce to the arguments 
 * passed in by the user.
 * 
 * @param int *thread_count - Output variable
 * Sets the thread count from program arguments
 * @param int argc - Input variable
 * Number of program arguments
 * @param char *argv[] - Input Variable
 * Array of program arguemnts
 * @return N/A No return value
 */
void Initialize(int *thread_count, int argc, char *argv[]);


/**
 * @brief Create_threads
 * Creates a number of threads that run the Collatz Function
 * 
 * @param pthread_t *threads - Output variable
 * Array to keep track of TIDs.
 * @param int thread_count - Input variable
 * Number of threads to create
 * @return N/A No return value
 */
void Create_threads(pthread_t *threads, int thread_count);


/** 
 * @brief Join_threads
 * Joins created threads with main process.
 * @param pthread_t *threads - Input variable
 * Array of TIDs to join.
 * @param int thread_count - Input variable
 * Number of threads to join
 * @return N/A No return value
 */
void Join_threads(pthread_t *threads, int thread_count);


/**
 * @brief collatz
 * Thread function. Uses two mutexs to lock COUNTER and HISTOGRAM 
 * variables. And calls upon functing Compute_stopping_time to calculate
 * stopping time for given value of COUNTER.
 * @param void *param - N/A No arguemnts needed
 * @return N/A No return value
 */
void *Collatz(void *param);


/**
 * @brief Compute_stopping_time
 * Computes the stopping time for the passed argument.
 * @param int num - Input variable
 * Number to compute the stopping time for.
 * @return int
 * Returns the stopping time for num using collatz conjecture.
 */
int Compute_stopping_time(int num);


/**
 * @brief Gen_plot_data
 * prints the histogram to stdout of csv style file.
 * Can be imported into Excel or MATLAB and plotted.
 * @param N/A
 * @return N/A
 */
void Gen_plot_data();

int HISTOGRAM[MAXSIZE] = {0};
int COUNTER = 2;
int COLLATZ_N;
int LOCK_FLAG = 1;
pthread_mutex_t CNTR_MUTEX,HISTO_MUTEX;

int main(int argc, char *argv[]){
    
    int thread_count;
    double run_time;
    struct timespec ts_start;
    struct timespec ts_end;
    
    Initialize(&thread_count, argc, argv);      /* Initialize and set variables */
    pthread_t threads[thread_count];            /* Initialize thread array */
    clock_gettime(CLOCK_REALTIME, &ts_start);   /* Get the start time */
    Create_threads(threads, thread_count);      /* Create threads */
    Join_threads(threads, thread_count);        /* Rejoin threads with main */
    clock_gettime(CLOCK_REALTIME, &ts_end);     /* Get stop time */
    Gen_plot_data();                            /* generate histogram data */
    run_time = (ts_end.tv_sec - ts_start.tv_sec) + (double)(ts_end.tv_nsec - ts_start.tv_nsec)/(double)1000000000;      /* calculate runtime in seconds */
    fprintf(stderr, "%i,%i,%lf\n", COLLATZ_N, thread_count, run_time);          /* print data of collatz, thread counts, and runtime to standard error */
    pthread_mutex_destroy(&CNTR_MUTEX);         /* Cleans up mutex */
    pthread_mutex_destroy(&HISTO_MUTEX);     
    
    return 0;
}

void Initialize(int *thread_count, int argc, char *argv[])
{
    
    if(argc < 3)                                /* checks whether user inputs the correct number of arguments for the program */
    {
        fprintf(stderr, "Illegal use of arguments. Program exiting.\n");
        exit(0);
    }
    COLLATZ_N = atoi(argv[1]);
    *thread_count = atoi(argv[2]);
    
    if(COLLATZ_N <= 0 || *thread_count <= 0)    /* checks to make sure both numbers are never negative or equal to 0 */
    {
        fprintf(stderr, "Collatz to compute or number of threads should be an integer greater than zero. Program exiting.\n");
        exit(0);
    }
    
    if(argc > 3 && strcasecmp(argv[3], "-nolock") == 0)
        LOCK_FLAG = 0;
    
    pthread_mutex_init(&CNTR_MUTEX,NULL);       /* initialize both mutex variables for COUNTER and HISTOGRAM[NUM] */
    pthread_mutex_init(&HISTO_MUTEX, NULL);
}

void Create_threads(pthread_t *threads, int thread_count){
    
    int i;
    
    for(i = 0; i < thread_count; i++)
        if(pthread_create(&threads[i], NULL, Collatz, NULL) != 0){
            fprintf(stderr, "Error creating threads. Program exiting.\n");
            exit(0);
        }
}

void Join_threads(pthread_t *threads, int thread_count){
    
    int i;
    
    for(i = 0; i < thread_count; i++)
        if(pthread_join(threads[i], NULL) != 0){
            fprintf(stderr, "Error joining threads. Program exiting.\n");
            exit(0);
        }
}

void *Collatz(void *param){
    
    (void) param;                                            /* Suppresses unused variable error */
    int local_hist[MAXSIZE] = {0};
    int count;
    while (1) {
        if(LOCK_FLAG){
            pthread_mutex_lock(&CNTR_MUTEX);                 /* lock COUNTER to prevent race conditions */
            if(COUNTER <= COLLATZ_N){        
                count = COUNTER;                            /* Record current COUNTER value */
                COUNTER++;                                  /* Incrament Counter */
                pthread_mutex_unlock(&CNTR_MUTEX);          /* Unlock Counter */
                local_hist[Compute_stopping_time(count)]++; /* calculate and recordstopping time for each sequence */
             }
             else{
                pthread_mutex_unlock(&CNTR_MUTEX);          /* Break out if no computation needed */
                break;
             }
        }
        else{
            if(COUNTER <= COLLATZ_N){                        /* Same as above but without locks */
                count = COUNTER;
                COUNTER++;
                local_hist[Compute_stopping_time(count)]++;
            }
            else
                break;
        }
    }
    if(LOCK_FLAG){
        pthread_mutex_lock(&HISTO_MUTEX);               /* Locks HISTOGRAM */
        for(count = 0; count < MAXSIZE; count++)
            HISTOGRAM[count] += local_hist[count];      /* Records results */
        pthread_mutex_unlock(&HISTO_MUTEX);             /* Unlocks HISTOGRAM */
    }
    else
        for(count = 0; count < MAXSIZE; count++)        /* Same as above but without locks */
            HISTOGRAM[count] += local_hist[count];
        
   return NULL;
}

int Compute_stopping_time(int num){
    
    int ans = 0;
    if(num == 1)
        return 0;
                                                            /**
                                                            * checks if num is even, and otherwise odd
                                                            * and compute stop time based on that until
                                                            * function reaches 1.
                                                            */
    if(num%2 == 0)
        ans = 1 + Compute_stopping_time(num/2);
    else
        ans = 1 + Compute_stopping_time((3*num + 1));
    return ans;
}

void Gen_plot_data(){
    
    int i;
    for(i = 1; i < MAXSIZE; i++)
        printf("%i,%i\n", i, HISTOGRAM[i]);
}
