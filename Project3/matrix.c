#include <stdio.h>
#include <time.h>

#define M 20480
#define N 4096
#define MAX_RUNS 10

double Row_Test(int w);
double Column_Test(int w);

char GLOBAL_ARRAY[M][N] = {0};


int main (void){
    
    double wr_runtime = 0 ,wc_runtime = 0 ,rr_runtime = 0 ,rc_runtime = 0 ;
    
    wr_runtime = Row_Test(1);
    wc_runtime = Column_Test(1);
    rr_runtime = Row_Test(0);
    rc_runtime = Column_Test(0);
    
    printf("Results are as follows:\nWrite Row:%lf\nWrite Column:%lf\nRead Row:%lf\nRead Column:%lf\n",wr_runtime,wc_runtime,rr_runtime,rc_runtime);
    
    return 0;
}



double Row_Test(int w){
    
    struct timespec ts_start;
    struct timespec ts_end;
    
    int counter,m,n,temp;
    double runtime=0;
    
    if (w){
        for(counter = 0; counter < MAX_RUNS; counter++){
            clock_gettime(CLOCK_REALTIME, &ts_start); 
            for(m = 0; m < M; m++)
                for(n = 0; n < N; n++)
                    GLOBAL_ARRAY[m][n]++;
            clock_gettime(CLOCK_REALTIME, &ts_end);
            runtime += (ts_end.tv_sec - ts_start.tv_sec) + (double)(ts_end.tv_nsec - ts_start.tv_nsec)/(double)1000000000;
        }
    }
    
    else{
        for(counter = 0; counter < MAX_RUNS; counter++){
            clock_gettime(CLOCK_REALTIME, &ts_start); 
            for(m = 0; m < M; m++)
                for(n = 0; n < N; n++)
                    temp = GLOBAL_ARRAY[m][n];
            clock_gettime(CLOCK_REALTIME, &ts_end);
            runtime += (ts_end.tv_sec - ts_start.tv_sec) + (double)(ts_end.tv_nsec - ts_start.tv_nsec)/(double)1000000000;
        }
    }
    temp += 2;
    return runtime/MAX_RUNS;
        
}

double Column_Test(int w){
    
    struct timespec ts_start;
    struct timespec ts_end;
    
    int counter,m,n,temp;
    double runtime=0;
    
    if (w){
        for(counter = 0; counter < MAX_RUNS; counter++){
            clock_gettime(CLOCK_REALTIME, &ts_start); 
            for(n = 0; n < N; n++)
                for(m = 0; m < M; m++)
                    GLOBAL_ARRAY[m][n]++;
            clock_gettime(CLOCK_REALTIME, &ts_end);
            runtime += (ts_end.tv_sec - ts_start.tv_sec) + (double)(ts_end.tv_nsec - ts_start.tv_nsec)/(double)1000000000;
        }
    }
    
    else{
        for(counter = 0; counter < MAX_RUNS; counter++){
            clock_gettime(CLOCK_REALTIME, &ts_start); 
            for(n = 0; n < N; n++)
                for(m = 0; m < M; m++)
                    temp = GLOBAL_ARRAY[m][n];
            clock_gettime(CLOCK_REALTIME, &ts_end);
            runtime += (ts_end.tv_sec - ts_start.tv_sec) + (double)(ts_end.tv_nsec - ts_start.tv_nsec)/(double)1000000000;
        }
    }
    temp += 2;
    return runtime/MAX_RUNS;
        
}
