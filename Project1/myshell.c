/***************************************************************************
 * File name:                                             *
 *  myshell.c                                             *
 *                                                        *
 * Purpose:                                               *
 *  Main driver for myshell program                       *
 *                                                        *
 * Authors:                                               *
 * -----------                                            *
 * Annaly Benitez                                         *
 * Taylor Lookabaugh                                      *
 * Andrew Petrovsky                                       *
 *                                                        *
 * Date:                                                  *
 *  19 Sept 2017                                          *
 *                                                        *
 * Course:                                                *
 *  COP4634                                               *
 *                                                        *
 * Notes:                                                 *
 *  May cause GPA to exceed 4.0                           *
 *********************************************************/
#define MAXBUFF 255                                                     /* Sets the maximum character length per command */

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include "parse.h"

/**********************************************************
 * Function Name:                                         *
 *  main                                                  *
 *                                                        *
 * Description:                                           *
 *  Main driver for the myshell program                   *
 *                                                        *
 * Arguments:                                             *
 *                                                        *
 * Argument       Type      I/O       Description         *
 * --------      -----     -----     -------------        *
 * -Debug         char      I         Enabled argument    *
 *                                      printout          *
 *                                                        *
 * Function Return:                                       *
 *  Always 0. Crashed return is undefined                 *
 *********************************************************/
int main(int argc, char *argv[])
{
    /*************************************************
     * Declaring and initilizing variables           * 
     ************************************************/
    char input_buffer[MAXBUFF]; 
    char *cmd_buffer[MAXARGS];
    int debug_flag = 0;
    int child_status;
    
    Param_t my_params; 
    pid_t pid;
    if(argc > 1 && strcasecmp(argv[1],"-debug") == 0){
        printf("DEBUG ON\n");
        debug_flag = 1;                                            /* Checks for the debug flag as input argument and sets flag */
    }
    while(1){
        Get_input(MAXBUFF,input_buffer,&my_params,debug_flag,cmd_buffer); 
        
        if(my_params.argumentVector[0] == NULL)                   /* If user just pressed enter, loop continues */
            continue;
        
        if (!strcmp(my_params.argumentVector[0], "exit")){       /* Checks for an exit command, and verrifies no children spawned */
            if (!waitpid(-1, &child_status,WNOHANG)){
                printf("Unable to exit background process running\n");
                continue;
            }
            else
                exit(0);
        }
        
        pid = fork();                                           /* Forks the process to create a child */
        if (!pid){
            if (my_params.inputRedirect != NULL)                 /* Sets input redirect if present */
                freopen(my_params.inputRedirect,"r",stdin);
            if (my_params.outputRedirect != NULL)
                freopen(my_params.outputRedirect,"w",stdout);   /* Sets output redirect if present */

            execvp(my_params.argumentVector[0],cmd_buffer);    /* Executes desired program with a null terminated string as argument */
            printf("myshell: command not found: %s\n", my_params.argumentVector[0]); /* Prints error message if program not found */
            exit(0);
        }
        if(!my_params.background)                              /* Waits for the child to terminate */
            waitpid(pid,&child_status,0);
    }
    return 0;
}
