/************************************************************
 * File name:                                               *
 *  parse.c                                                 *
 *                                                          *
 * Purpose:                                                 *
 *  Parser source file. Contains the functions to tokenize  *
 *  input from stdin, set redirect and background flags,    *
 *  and print out arguemnts when shell is started with      *
 *  -debug flag.                                            *
 *                                                          *
 * Authors:                                                 *
 * -----------                                              *
 * Annaly Benitez                                           *
 * Taylor Lookabaugh                                        *
 * Andrew Petrovsky                                         *
 *                                                          *
 * Date:                                                    *
 *  19 Sept 2017                                            *
 *                                                          *
 * Course:                                                  *
 *  COP4634                                                 *
 *                                                          *
 * Notes:                                                   *
 *  Some functions are not in the header file because       *
 *  obstraction and obfuscation.                            *
 ***********************************************************/
#include <string.h>
#include <stdio.h>
#include "parse.h"

    /*************************************************
     * Function name: printParams                    * 
     *  Provided function, used to printout arguments*
     ************************************************/
void printParams(Param_t * param);

/**********************************************************
 * Function Name:                                         *
 *  Check_background                                      *
 *                                                        *
 * Description:                                           *
 *  Checks for the background symbol (&). Sets appropriate*       
 *  flags if found.                                       *
 *                                                        *
 * Arguments:                                             *
 *                                                        *
 * Argument       Type      I/O       Description         *
 * --------      -----     -----     -------------        *
 * param_str     Param_t    I/O      Checks current       *
 *                                     argcount, and sets *
 *                                     background flags   *
 * Function Return:                                       *
 *  Returns 1 if symbol has been found. Else 0.           *
 *********************************************************/
int Check_background(Param_t *param_str);

/**********************************************************
 * Function Name:                                         *
 *  Check_length                                          *
 *                                                        *
 * Description:                                           *
 *  Checks to see if current argument is concatenated     *
 *  with > or <                                           *       
 *                                                        *
 * Arguments:                                             *
 *                                                        *
 * Argument       Type      I/O       Description         *
 * --------      -----     -----     -------------        *
 * param_str     Param_t    I/O      Checks and modifies  *
 *                                     structure members  *
 * option        int         I       Is used to set type  *
 *                                      1 = input         *  
 *                                      0 = output        *  
 * Function Return:                                       *
 *  Always returns 1                                      *
 *********************************************************/
int Check_length(Param_t *param_str,int option);

/**********************************************************
 * Function Name:                                         *
 *  Check_redirect                                        *
 *                                                        *
 * Description:                                           *
 *  Checks to see if current argument is > or < and sets  *
 *  the proper arguemnts                                  *       
 *                                                        *
 * Arguments:                                             *
 *                                                        *
 * Argument       Type      I/O       Description         *
 * --------      -----     -----     -------------        *
 * param_str     Param_t    I/O      Checks and modifies  *
 *                                      structure members *
 * option        int         I       Is used to set args  *
 *                                      that are seperated*  
 *                                      by space. See     *  
 *                                      return values     *
 * Function Return:                                       *
 *  0 when not found                                      *
 *  1 when action need for outputRedirect                 *
 *  2 when action need for outputRedirect                 *
 *  3 action taken and needs next token                   *
 *********************************************************/
int Check_redirect(Param_t *param_str,int option);

/**********************************************************
 * Function Name:                                         *
 *  Get_input                                             *
 *                                                        *
 * Description:                                           *
 *  Provides a prompt and parses input received from stdin*
 *                                                        *
 * Arguments:                                             *
 *                                                        *
 * Argument       Type      I/O       Description         *
 * --------      -----     -----     -------------        *
 * buff_size      int        I      Set maximum size of   *
 *                                    input buffer        *
 * buffer         char*      O      Array to store input  *
 *                                    from stdin          *
 * my_params      Param_t*   O      Parameter struct      *
 * debug_flag       int      I      Debug mode flag       *
 *                                    prints out args if  *
 *                                    set to 1            *
 * cmd_list       char**     O      Char array for command*
 *                                   arguemnts            *
 *                                                        *
 * Function Return:                                       *
 *  No return value                                       *
 *********************************************************/
void Get_input(int buffsize, char *buff,Param_t *params_str,int dbgflag,char **cmd_list){
    /************************************************
     * Setting up variables so the passed in        * 
     * paramaters structure is cleanfrom last use.  *
     ************************************************/
    int flag = 0; 
    int cmd_count = 0;
    params_str->inputRedirect = NULL; 
    params_str->outputRedirect = NULL; 
    params_str->background = 0; 
    params_str->argumentCount = 0; 
    
    /************************************************
     * Printing console message and getting input   * 
     * from stdin                                   *
     ************************************************/
    printf("$$"); 
    fgets(buff,buffsize,stdin); 
    
    /************************************************
     * First pass of strtok, adding the command to  *
     * command list.                                *
     ************************************************/
    params_str->argumentVector[0] = strtok(buff, " \t\n"); 
    params_str->argumentCount++; 
    cmd_list[0] = params_str->argumentVector[0];
    cmd_count++;
    
    /************************************************
     * While loop to tokenize the remainder of the  *
     * input buffer. This loop also takes care of   *
     * redirection and background processing.       *
     ************************************************/
    while((params_str->argumentVector[params_str->argumentCount]= strtok(NULL," \t\n")) != NULL)
    {
        if ((flag = Check_redirect(params_str,flag)))           
            continue;
        if ((Check_background(params_str)))                     
            continue;

        cmd_list[cmd_count] = params_str->argumentVector[params_str->argumentCount];        /* Adds argument to command list for use by main */
        cmd_count++;
        params_str->argumentCount++;
    };
    
    cmd_list[cmd_count] = NULL;                                                             /* Adds a null terminator to end of command list */
    
    if (dbgflag)                                                                            /* Prints arguments if shell started with debug flag */
        printParams(params_str);

}
int Check_redirect(Param_t *param_str,int option){
    /************************************************
     * Sets the return to either 1, 2 or 3. Via     *
     * return from Check_length.                    *
     *                                              *
     * Given that:                                  *
     * outputRedirect = 1                           *
     * inputRedirect = 2                            *
     * nothing to do = 3                            *
     *                                              *
     * This function, when in a loop, will resolve  *
     * itself until the return value is 0 and the   *
     * while loop is not forced to continue.        *
     ************************************************/
    
    if (*param_str->argumentVector[param_str->argumentCount] == '>')
        return Check_length(param_str,0) ? 1 : 3;                                           
    if (*param_str->argumentVector[param_str->argumentCount] == '<')                        
        return Check_length(param_str,1) ? 2 : 3;                                           
    
    if(option == 1){
        param_str->outputRedirect = param_str->argumentVector[param_str->argumentCount];
        param_str->argumentCount++;
        return 3;
    }
    if (option == 2){
        param_str->inputRedirect = param_str->argumentVector[param_str->argumentCount];
        param_str->argumentCount++;
        return 3;
    }

    return 0;
}

int Check_length(Param_t *param_str,int option){
    /************************************************
     * Argument length will be checked to see if    *
     * redirection flag is concatenated with the    *
     * filename.                                    * 
     ************************************************/
    if (strlen(param_str->argumentVector[param_str->argumentCount]) > 1)
    {
        if (option)
            param_str->inputRedirect = param_str->argumentVector[param_str->argumentCount]+1;   /* inputRedirect is set w/ offset */
        else 
            param_str->outputRedirect = param_str->argumentVector[param_str->argumentCount]+1;  /* outputRedirect is set w/ offset */
        
        param_str->argumentCount++;
        return 1;
    }
    else
    {
        param_str->argumentCount++;                                                       /* Symbol by itself, will set next cycle */
        return 1;
    }
}
int Check_background(Param_t *param_str){
    if (*param_str->argumentVector[param_str->argumentCount] == '&')
    {
        param_str->background = 1;
        param_str->argumentCount++;
        return 1;
    }
    return 0;
}
void printParams(Param_t * param){
    int i;
    printf ("InputRedirect: [%s]\n",(param->inputRedirect != NULL) ? param->inputRedirect:"NULL");
    printf ("OutputRedirect: [%s]\n",(param->outputRedirect != NULL) ? param->outputRedirect:"NULL");
    printf ("Background: [%d]\n", param->background);
    printf ("ArgumentCount: [%d]\n", param->argumentCount);
    for (i = 0; i < param->argumentCount; i++)
        printf("ArgumentVector[%2d]: [%s]\n", i, param->argumentVector[i]);
}
