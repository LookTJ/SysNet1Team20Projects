/************************************************************
 * File name:                                               *
 *  parse.h                                                 *
 *                                                          *
 * Purpose:                                                 *
 *  Parser header file. Contains the functions to tokenize  *
 *  input from stdin, set redirect and background flags,    *
 *  and print out arguemnts when shell is started with      *
 *  -debug flag.                                            *
 *                                                          *
 * Authors:                                                 *
 * -----------                                              *
 * Annaly Benitez                                           *
 * Taylor Lookabaugh                                        *
 * Andrew Petrovsky                                         *
 *                                                          *
 * Date:                                                    *
 *  19 Sept 2017                                            *
 *                                                          *
 * Course:                                                  *
 *  COP4634                                                 *
 *                                                          *
 * Notes:                                                   *
 *  Some functions are not in the header file because       *
 *  obstraction and obfuscation.                            *
 ***********************************************************/
#ifndef PARSE_H
#define PARSE_H
#define MAXARGS 32

    /*************************************************
     * Structure name: PARAM                         * 
     *  Provided structure, used to store arguments  *
     ************************************************/
struct PARAM
{
  char *inputRedirect;
  char *outputRedirect;
  int background;
  int argumentCount;
  char *argumentVector[MAXARGS];
};
typedef struct PARAM Param_t;

/**********************************************************
 * Function Name:                                         *
 *  Get_input                                             *
 *                                                        *
 * Description:                                           *
 *  Provides a prompt and parses input received from stdin*
 *                                                        *
 * Arguments:                                             *
 *                                                        *
 * Argument       Type      I/O       Description         *
 * --------      -----     -----     -------------        *
 * buff_size      int        I      Set maximum size of   *
 *                                    input buffer        *
 * buffer         char*      O      Array to store input  *
 *                                    from stdin          *
 * my_params      Param_t*   O      Parameter struct      *
 * debug_flag       int      I      Debug mode flag       *
 *                                    prints out args if  *
 *                                    set to 1            *
 * cmd_list       char**     O      Char array for command*
 *                                   arguemnts            *
 *                                                        *
 * Function Return:                                       *
 *  No return value                                       *
 *********************************************************/
void Get_input(int buff_size,char *buffer, Param_t *my_params, int debug_flag,char **cmd_list);
#endif
